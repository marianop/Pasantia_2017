\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n}{4}
\contentsline {subsection}{\numberline {1.1}Datos personales}{5}
\contentsline {subsubsection}{\numberline {1.1.1}Estudiante}{5}
\contentsline {subsubsection}{\numberline {1.1.2}Tutor Acad\IeC {\'e}mico - Industrial}{5}
\contentsline {subsection}{\numberline {1.2}Instituci\IeC {\'o}n donde se realizo la Pasant\IeC {\'\i }a}{6}
\contentsline {section}{\numberline {2}Descripci\IeC {\'o}n}{7}
\contentsline {subsection}{\numberline {2.1}Objetivos}{7}
\contentsline {subsubsection}{\numberline {2.1.1}Generales}{7}
\contentsline {subsubsection}{\numberline {2.1.2}Espec\IeC {\'\i }ficos}{7}
\contentsline {subsection}{\numberline {2.2}Procesamiento digital de Imagen}{8}
\contentsline {subsubsection}{\numberline {2.2.1}Transformaci\IeC {\'o}n de Im\IeC {\'a}genes}{8}
\contentsline {subsubsection}{\numberline {2.2.2}Algoritmo Beier y Neely\IeC {\textquoteright }s}{12}
\contentsline {subsubsection}{\numberline {2.2.3}Algoritmo Polyline}{15}
\contentsline {subsection}{\numberline {2.3}Herramientas de trabajo}{15}
\contentsline {subsection}{\numberline {2.4}Actividades realizadas}{16}
\contentsline {subsubsection}{\numberline {2.4.1}Plan de trabajo}{16}
\contentsline {subsubsection}{\numberline {2.4.2}Cronograma de actividades}{16}
\contentsline {subsubsection}{\numberline {2.4.3}Descripci\IeC {\'o}n detallada de las actividades}{16}
\contentsline {section}{\numberline {3}Metodolog\IeC {\'\i }a}{16}
\contentsline {subsection}{\numberline {3.1}An\IeC {\'a}lisis e implementaci\IeC {\'o}n}{16}
\contentsline {subsection}{\numberline {3.2}Ejemplos Morphing Beier \& Neely\IeC {\textquoteright }s}{17}
\contentsline {subsubsection}{\numberline {3.2.1}1- Segmento}{17}
\contentsline {subsubsection}{\numberline {3.2.2}2- Segmentos}{19}
\contentsline {subsubsection}{\numberline {3.2.3}3- Segmentos}{21}
\contentsline {subsubsection}{\numberline {3.2.4}4- Segmentos}{23}
\contentsline {subsubsection}{\numberline {3.2.5}N- Segmentos}{24}
\contentsline {subsection}{\numberline {3.3}Ejemplos Morphing Polyline}{25}
\contentsline {subsubsection}{\numberline {3.3.1}1- Segmentos}{25}
\contentsline {subsubsection}{\numberline {3.3.2}2- Segmentos}{25}
\contentsline {subsubsection}{\numberline {3.3.3}3- Segmentos}{25}
\contentsline {subsubsection}{\numberline {3.3.4}4- Segmentos}{25}
\contentsline {subsubsection}{\numberline {3.3.5}N- Segmentos}{25}
\contentsline {subsection}{\numberline {3.4}Comparaciones}{25}
\contentsline {subsection}{\numberline {3.5}Resultados}{25}
\contentsline {section}{\numberline {4}Conclusiones}{25}
\contentsline {subsection}{\numberline {4.1}Discusi\IeC {\'o}n}{25}
\contentsline {subsection}{\numberline {4.2}Mejoras}{25}
\contentsline {section}{\numberline {5}Informe de la instituci\IeC {\'o}n sobre las actividades del estudiante}{25}
\contentsline {subsection}{\numberline {5.1}Fecha de inicio y fin de las pr\IeC {\'a}cticas}{25}
\contentsline {subsection}{\numberline {5.2}Asistencia a la instituci\IeC {\'o}n para realizar las pr\IeC {\'a}cticas}{25}
\contentsline {subsection}{\numberline {5.3}Rendimiento profesional del estudiante}{25}
\contentsline {subsection}{\numberline {5.4}Informe t\IeC {\'e}cnico de la instituci\IeC {\'o}n}{25}
\contentsline {section}{\numberline {6}Bibliograf\IeC {\'\i }a}{26}
